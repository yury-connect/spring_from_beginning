package com.kciray;


import org.springframework.beans.factory.stereotype.Component;


/*
ProductService будет возвращать продукт из БД,
но перед этим нужно проверить, применимы ли к этому продукту какие-либо скидки (Promotions)

Используем самый современный подход (сканирование классов и аннотации).
Для этого нам нужно ручками создать аннотацию
@Component (пакет org.springframework.beans.factory.stereotype)
 */
@Component
public class ProductService {
    private PromotionsService promotionsService;

    public PromotionsService getPromotionsService() {
        return promotionsService;
    }

    public void setPromotionsService(PromotionsService promotionsService) {
        this.promotionsService = promotionsService;
    }
}
