package org.springframework.beans.factory;

import java.util.HashMap;
import java.util.Map;

/* КОНТЕЙНЕР *
BeanFactory - фабрика бинов (контейнер);
FactoryBean - бин-фабрика, которая сидит внутри контейнера и тоже производит бины. Фабрика внутри фабрики.;
 */
public class BeanFactory {
    private Map<String, Object> singletons = new HashMap();

    public Object getBean(String beanName){
        return singletons.get(beanName);
    }

}
